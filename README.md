# Chatroom protocol

There are 2 connections between the server and the client:

- _Action connection_:
  - the client sends requests (like _login_, _send_),
  - the server sends responses them
- _Event connection_:
  - the server sends events (like _message recieved_) to the clients

## Action connection (port `8080`)

### Login

_Request_

`{ "action" : "login", "name" : "<nickname>" }`

_Response_

`{ "success" : true, "id" : <id>}`

### Send message

_Request_

`{ "id" : <id>, "action" : "send", "msgid" : <msgid>, "message" : "<msg>" }`

_Response_

`{ "success" : <true/false>, "message": "<success/error message>" }`

### Shake window

_Request_

`{ "id" : <id>, "action" : "shakewindow" }`

_Response_

`{ "success" : <true/false>, "message": "<success/error message>" }`

### List of online users

_Request_

`{ "id" : <id>, "action" : "onlineusers" }`

_Response_

`{ "success" : <true/false>, "onlineusers" : [ {"id" : <onlineuserid1>, "name" : "<onlineuser1>"}, {"id" : <onlineuserid2>, "name" : "<onlineuser2>"}, … ] }`

## Event connection (port `8081`)

### List of online users changed

`{ "eventtype" : "onlineuserschanged" }`

### Message recieved

`{ "eventtype" : "newmessage", "senderid" : <senderid>, "msgid" : <msgid>, "message" : <message> }`

### Shake window

`{ "eventtype" : "shakewindow" }`